package com.love.student_cs;

import com.love.student_cs.entity.Administrator;
import com.love.student_cs.mapper.AdministratorMapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@SpringBootApplication
public class StudentCsApplication {



    public static void main(String[] args) {
        SpringApplication.run(StudentCsApplication.class, args);
    }



}
