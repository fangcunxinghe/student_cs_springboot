package com.love.student_cs.common;

public interface Constants {
//    成功
    String CODE_200 ="200";
//    系统错误
    String CODE_500="500";
//    权限不足
    String CODE_401="401";
//    参数错误
    String CODE_400="400";
//    用户名密码错误
    String CODE_501="501";
}
