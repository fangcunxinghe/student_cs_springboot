package com.love.student_cs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.love.student_cs.entity.CourseSelection;
import com.love.student_cs.mapper.CourseSelectionMapper;
import org.springframework.stereotype.Service;

@Service
public class CourseSelectionService extends ServiceImpl<CourseSelectionMapper, CourseSelection> implements IService<CourseSelection> {
}
