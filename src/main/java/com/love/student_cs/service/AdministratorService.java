package com.love.student_cs.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.log.Log;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.love.student_cs.common.Constants;
import com.love.student_cs.controller.dto.AdministratorDTO;
import com.love.student_cs.entity.Administrator;
import com.love.student_cs.exception.ServiceException;
import com.love.student_cs.mapper.AdministratorMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Service
public class AdministratorService extends ServiceImpl<AdministratorMapper, Administrator> {



    private static final Log LOG=Log.get();
//    更新插入
    public boolean adminsave(Administrator administrator) {
//        if (administrator.getAid()==null){
//            return save(administrator);
//        }else {
//            return updateById(administrator);
//        }
        return saveOrUpdate(administrator);
    }



    public AdministratorDTO login(AdministratorDTO administrator) {
        QueryWrapper<Administrator> queryWrapper=new  QueryWrapper<>();
        queryWrapper.eq("username",administrator.getUsername());
        queryWrapper.eq("password",administrator.getPassword());
        try {
            Administrator admin=getOne(queryWrapper);
            if (admin!=null){
                BeanUtil.copyProperties(admin,administrator,true);
                return administrator;
            }else {
                throw new ServiceException(Constants.CODE_501,"用户名或密码错误");
            }
        }catch (Exception e){
            LOG.error(e);
            throw new ServiceException(Constants.CODE_500,"系统错误");
        }
    }
}
