package com.love.student_cs.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.love.student_cs.entity.Administrator;
import com.love.student_cs.entity.Student;
import com.love.student_cs.mapper.StudentMapper;
import org.springframework.stereotype.Service;

@Service
public class StudentService extends ServiceImpl<StudentMapper, Student> implements IService<Student> {
    public boolean login(Student student) {
        QueryWrapper<Student> queryWrapper=new  QueryWrapper<>();
        queryWrapper.eq("sname",student.getName());
        queryWrapper.eq("spassword",student.getPassword());
        Student stu=getOne(queryWrapper);
        return stu != null;
    }
}
