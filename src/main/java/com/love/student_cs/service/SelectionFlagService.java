package com.love.student_cs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.love.student_cs.entity.SelectionFlag;
import com.love.student_cs.mapper.SelectionFlagMapper;
import org.springframework.stereotype.Service;

@Service
public class SelectionFlagService extends ServiceImpl<SelectionFlagMapper, SelectionFlag> implements IService<SelectionFlag> {
}
