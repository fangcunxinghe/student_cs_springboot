package com.love.student_cs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.love.student_cs.entity.Administrator;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface AdministratorMapper extends BaseMapper<Administrator>{

}
