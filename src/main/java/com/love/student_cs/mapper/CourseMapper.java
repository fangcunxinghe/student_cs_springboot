package com.love.student_cs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.love.student_cs.entity.Course;

public interface CourseMapper extends BaseMapper<Course> {
}
