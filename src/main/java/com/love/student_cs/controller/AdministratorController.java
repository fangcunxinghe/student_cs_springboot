package com.love.student_cs.controller;

import cn.hutool.core.util.StrUtil;
import com.love.student_cs.common.Constants;
import com.love.student_cs.common.Result;
import com.love.student_cs.controller.dto.AdministratorDTO;
import com.love.student_cs.entity.Administrator;
import com.love.student_cs.mapper.AdministratorMapper;
import com.love.student_cs.service.AdministratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/admin")
public class AdministratorController {


    @Autowired
    private AdministratorService administratorService;


   //查询全部
    @GetMapping("/all")
    public List<Administrator> FindAll(){
        return administratorService.list();
    }

    //    插入更新
    @PostMapping("/insert")
    public boolean SaveAdmin(@RequestBody Administrator administrator){
        return administratorService.adminsave(administrator);
    }

//    删除
    @DeleteMapping("/delete/{id}")
    public boolean DeleteAdmin(@PathVariable Integer id){
        return administratorService.removeById(id);
    }


    @GetMapping("/find/{id}")
    public Administrator getAdminById(@PathVariable int id){
        Administrator admin=administratorService.getById(id);
        return admin;
    }


    //    登陆
    @PostMapping("/login")
    public Result login(@RequestBody AdministratorDTO administrator){
        String name= administrator.getUsername();
        String password =administrator.getPassword();
        if (StrUtil.isBlank(name)||StrUtil.isBlank(password)){
            return Result.error(Constants.CODE_400,"参数错误");
        }
        AdministratorDTO administratorDTO=administratorService.login(administrator);
        return Result.success(administratorDTO);
    }

}
