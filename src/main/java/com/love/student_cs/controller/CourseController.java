package com.love.student_cs.controller;

import com.love.student_cs.entity.Administrator;
import com.love.student_cs.entity.Course;
import com.love.student_cs.entity.Student;
import com.love.student_cs.service.CourseService;
import com.love.student_cs.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/course")
public class CourseController {
    @Autowired
    private CourseService courseService;


    //查询全部
    @GetMapping("/all")
    public List<Course> FindAll(){
        return courseService.list();
    }

    //    插入更新
    @PostMapping("/insert")
    public boolean SaveCou(@RequestBody Course course){
        return courseService.saveOrUpdate(course);
    }

    //    删除
    @DeleteMapping("/delete/{id}")
    public boolean DeleteCou(@PathVariable Integer id){
        return courseService.removeById(id);
    }

//    id查询
    @GetMapping("/find/{id}")
    public Course getCouById(@PathVariable int id){
        Course course=courseService.getById(id);
        return course;
    }

}
