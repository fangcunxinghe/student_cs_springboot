package com.love.student_cs.controller;

import cn.hutool.core.util.StrUtil;
import com.love.student_cs.entity.Administrator;
import com.love.student_cs.entity.Student;
import com.love.student_cs.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/student")
public class StudentController {

    @Autowired
    private StudentService studentService;


    //查询全部
    @GetMapping("/all")
    public List<Student> FindAll(){
        return studentService.list();
    }

    //    插入更新
    @PostMapping("/insert")
    public boolean SaveStu(@RequestBody Student student){
        return studentService.saveOrUpdate(student);
    }

    //    删除
    @DeleteMapping("/delete/{id}")
    public boolean DeleteStu(@PathVariable Integer id){
        return studentService.removeById(id);
    }

    //    id查询
    @GetMapping("/find/{id}")
    public Student getStuById(@PathVariable int id){
        Student student=studentService.getById(id);
        return student;
    }

    //    登陆
    @PostMapping("/login")
    public boolean login(@RequestBody Student student){
        String name= student.getName();
        String password =student.getPassword();
        if (StrUtil.isBlank(name)||StrUtil.isBlank(password)){
            return false;
        }
        return studentService.login(student);
    }

}
