package com.love.student_cs.controller.dto;


import lombok.Data;

//接受前端登陆请求参数

@Data
public class AdministratorDTO {

    private Integer aid;/*管理员Id*/
    private String username;/*管理员用户名*/
    private String password;/*管理员密码*/



//    管理员登录
//    请求接口：/admin/login
//    传入：
//    {
//        username(用户名)
//        password（密码）
//    }
//    返回（200）：
//            1.登录成功：
//    {
//        msg:"successful",
//                token(登录凭证)，
//        aid（管理员id），
//        username（管理员用户名），
//        role:"admin"
//    }
//2.登录失败：
//    {
//        msg:"unsuccessful"
//    }
}
