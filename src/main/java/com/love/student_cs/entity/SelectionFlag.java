package com.love.student_cs.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 选课开关
 */
@Data
@TableName("selection_flag")
public class SelectionFlag implements Serializable {
    @TableId(type = IdType.AUTO)
    private Integer fid;/*唯一自增主键*/
    private Date submitTime;/*任务提交时间*/
    private Date startTime;/*开始时间*/
    private Date endTime;/*结束时间*/
    private Integer flag;/*开关：0关，1开*/
}
