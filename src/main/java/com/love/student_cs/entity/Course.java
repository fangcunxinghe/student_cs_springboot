package com.love.student_cs.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 课程类
 */
@Data
@TableName("courses")
public class Course implements Serializable {
    @TableId
    private String cid;/*课程编号*/
    private String term;/*修读学期*/
    private String name;/*课程名*/
    private String teacher;/*授课教师*/
    private Integer period;/*总学时*/
    @TableField(value = "score")
    private Integer credit;/*学分*/
    private String location;/*授课地点*/
    private String week;/*周次*/
    private Integer section;/*课程节次*/
    private String attr;/*课程属性*/
    private Integer capacity;/*课容量*/
    private Integer selectNum;/*选课人数*/
}