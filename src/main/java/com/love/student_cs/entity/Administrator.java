package com.love.student_cs.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 管理员类
 */
@Data
@TableName("administrators")
public class Administrator implements Serializable {
    @TableId(type = IdType.AUTO)
    private Integer aid;/*管理员Id*/
    private String username;/*管理员用户名*/
    private String password;/*管理员密码*/
}