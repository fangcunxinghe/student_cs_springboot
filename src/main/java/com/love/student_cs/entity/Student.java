package com.love.student_cs.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 学生类
 */
@Data
@TableName("students")
public class Student implements Serializable {
    @TableId
    private Long sid;/*学号*/
    private String name;/*姓名*/
    private String gender;/*性别*/
    private Integer grade;/*年级*/
    private String academy;/*学院*/
    private String major;/*专业*/
    private String className;/*班级名*/
    private String password;/*密码*/

}
