package com.love.student_cs.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 选课记录
 */
@Data
@TableName("course_selection")
public class CourseSelection implements Serializable {
    @TableId(type = IdType.AUTO)
    private Integer no;/*唯一自增主键*/
    private Long sid;/*学生id*/
    private String cid;/*课程id*/
    private Date selectedTime;/*选课时间*/
    private Integer score;/*课程成绩*/
}
